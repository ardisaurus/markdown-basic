# Tittle

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

---

### Heading

# Heading 1
## Heading 2
### Heading 3
##### Heading 4
###### Heading 5

---

### Text formating

Dillinger is a cloud-enabled, mobile-ready, offline-storage, AngularJS powered HTML5 Markdown editor

I just love **bold text**.

Italicized text is the *cat's meow*.

Dillinger requires [Node.js](https://nodejs.org/) v4+ to run.

<https://www.markdownguide.org>
<fake@example.com>

> The overriding design goal for Markdown's
> formatting syntax is to make it as readable
> as possible.

---

### Bullet and Numbering

- Type some Markdown on the left
- See HTML in the right
  - Magic

1. First item
2. Second item
3. Third item
4. Fourth item 

---

### Codes

```sh
$ cd dillinger
$ npm install -d
$ node app
```

At the command prompt, type `nano`.

---

### Table

| Plugin           | README                                    |
| ---------------- | ----------------------------------------- |
| Dropbox          | [plugins/dropbox/README.md][pldb]         |
| Github           | [plugins/github/README.md][plgh]          |
| Google Drive     | [plugins/googledrive/README.md][plgd]     |
| OneDrive         | [plugins/onedrive/README.md][plod]        |
| Medium           | [plugins/medium/README.md][plme]          |
| Google Analytics | [plugins/googleanalytics/README.md][plga] |
